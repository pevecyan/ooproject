module.exports={
    "year":[
        [
            {
                "id":"room_1_1",
                "title":"Belo mleko belih prsi",
                "type":"B",
                "year":"2010",
                "left":"",
                "top":"",
                "right":"room_2_10",
                "bottom":"room_1_2_3"
            },
            {
                "id":"room_1_2_3",
                "title":"72 h zime in 72 h jeseni",
                "type":"A",
                "year":"1969/2016",
                "left":"",
                "top":"room_1_1",
                "right":"room_1_2_2",
                "bottom":"room_1_2_2"
            },
            {
                "id":"room_1_2_2",
                "title":"72 h zime in 72 h jeseni",
                "type":"A",
                "year":"1969",
                "left":"room_1_2_3",
                "top":"room_1_2_3",
                "right":"room_1_2_1",
                "bottom":"room_1_2_1"
            },
            {
                "id":"room_1_2_1",
                "title":"72 h zime in 72 h jeseni",
                "type":"A",
                "year":"1969",
                "left":"room_1_2_2",
                "top":"room_1_2_2",
                "right":"room_1_7",
                "bottom":"room_1_7"
            },
            {
                "id":"room_1_7",
                "title":"Konceptna tabla VII",
                "type":"A",
                "year":"2010",
                "left":"room_1_2_1",
                "top":"room_1_2_1",
                "right":"room_1_8",
                "bottom":"room_1_8"
            },
            {
                "id":"room_1_8",
                "title":"Konceptna tabla VI",
                "type":"A",
                "year":"2010",
                "left":"room_1_7",
                "top":"room_1_7",
                "right":"room_2_3_1",
                "bottom":""
            }
        ],
        [
           {
                "id":"room_2_3_1",
                "title":"40 x 50 x 80 cm peska pod steklom",
                "type":"A",
                "year":"1969",
                "left":"room_1_8",
                "top":"",
                "right":"room_2_3_2",
                "bottom":"room_2_3_2"
            },
            {
                "id":"room_2_3_2",
                "title":"40 x 50 x 80 cm peska pod steklom",
                "type":"A",
                "year":"1969",
                "left":"room_2_3_1",
                "top":"room_2_3_1",
                "right":"room_2_9",
                "bottom":"room_2_9"
            },
            {
                "id":"room_2_9",
                "title":"Wizard",
                "type":"A",
                "year":"1970",
                "left":"room_2_3_2",
                "top":"room_2_3_2",
                "right":"room_2_11",
                "bottom":"room_2_10"
            },
            {
                "id":"room_2_10",
                "title":"Miselna vizualizacija objekta",
                "type":"B+C",
                "year":"1972",
                "left":"room_1_1",
                "top":"room_2_9",
                "right":"room_2_11:room_3_22",
                "bottom":"room_2_11"
            },
            {
                "id":"room_2_11",
                "title":"Smer relacije senzibilnost-imaginacija",
                "type":"A+B",
                "year":"1972",
                "left":"room_2_9:room_2_10",
                "top":"room_2_10",
                "right":"room_2_15",
                "bottom":"room_2_15"
            },
            {
                "id":"room_2_15",
                "title":"Konceptna tabla IX",
                "type":"A+B",
                "year":"1974",
                "left":"room_2_11",
                "top":"room_2_11",
                "right":"room_2_16",
                "bottom":"room_2_16"
            },
            {
                "id":"room_2_16",
                "title":"Alpha Theta Ritem ",
                "type":"A+B",
                "year":"1974",
                "left":"room_2_15",
                "top":"room_2_15",
                "right":"room_6_39:room_6_34",
                "bottom":""
            },
        ],
        [
            {
                "id":"room_3_18",
                "title":"Pokretne slike",
                "type":"F",
                "year":"1981",
                "left":"",
                "top":"",
                "right":"room_4_26",
                "bottom":"room_3_21"
            },
            {
                "id":"room_3_21",
                "title":"Spomenik žrtev in orgije: padec človeštva na odeških stopnicah",
                "type":"G",
                "year":"1985",
                "left":"",
                "top":"room_3_18",
                "right":"room_3_23",
                "bottom":"room_3_21"
            },
            {
                "id":"room_3_22",
                "title":"Video anamorfoza I, IX, III ",
                "type":"C",
                "year":"1987",
                "left":"room_2_10",
                "top":"room_3_21",
                "right":"room_3_23",
                "bottom":"room_3_23"
            },
            {
                "id":"room_3_23",
                "title":"Spomenik žrtev in orgije ",
                "type":"G+C",
                "year":"1985",
                "left":"room_3_21:room_3_23",
                "top":"room_3_22",
                "right":"room_3_24",
                "bottom":"room_3_24"
            },
            {
                "id":"room_3_24",
                "title":"Meščevo zlato",
                "type":"G+C",
                "year":"1987",
                "left":"room_3_23",
                "top":"room_3_23",
                "right":"room_4_25:room_5_28",
                "bottom":""
            }, 
        ],
        [
            {
                "id":"room_4_25",
                "title":"Gosposvetski sen",
                "type":"G",
                "year":"1987",
                "left":"room_3_23",
                "top":"",
                "right":"room_7_50",
                "bottom":"room_4_26"
            },
            {
                "id":"room_4_26",
                "title":"Konceptna tabla XI",
                "type":"F",
                "year":"",
                "left":"room_3_18",
                "top":"room_4_25",
                "right":"",
                "bottom":""
            },
        ],
        [
            {
                "id":"room_5_28",
                "title":"Madež",
                "type":"C",
                "year":"1991",
                "left":"room_3_24",
                "top":"",
                "right":"room_5_29",
                "bottom":"room_5_29"
            },
            {
                "id":"room_5_29",
                "title":"Opus Canum",
                "type":"C",
                "year":"1997",
                "left":"room_5_28",
                "top":"room_5_28",
                "right":"room_5_30",
                "bottom":"room_5_30"
            },
            {
                "id":"room_5_30",
                "title":"Konceptna tabla XV ",
                "type":"C",
                "year":"1997",
                "left":"room_5_29",
                "top":"room_5_29",
                "right":"room_5_32",
                "bottom":"room_5_31_1"
            },
            {
                "id":"room_5_31_1",
                "title":"Kaj je videoart? Intervju s Srečom Draganom",
                "type":"H",
                "year":"2002",
                "left":"",
                "top":"room_5_30",
                "right":"room_5_31_2",
                "bottom":"room_5_31_2"
            },
            {
                "id":"room_5_31_2",
                "title":"Video-integrirani mediji. Srečo Dragan: Rotas - Sator (1993-98), Čas, vržen iz tira (2000-)",
                "type":"H",
                "year":"2002",
                "left":"room_5_31_1",
                "top":"room_5_31_1",
                "right":"room_6_37",
                "bottom":"room_5_32"
            },
            {
                "id":"room_5_32",
                "title":"Konceptna tabla II",
                "type":"C",
                "year":"",
                "left":"room_5_30",
                "top":"room_5_31_2",
                "right":"room_6_34",
                "bottom":"room_5_33"
            },
             {
                "id":"room_5_33",
                "title":"Arheus (Piero della Francesca in virtualna umetnost) ",
                "type":"E",
                "year":"1992/93",
                "left":"",
                "top":"room_5_32",
                "right":"room_6_35",
                "bottom":""
            },
        ],
        [
            {
                "id":"room_6_34",
                "title":"Metaforične razširitve",
                "type":"B+C",
                "year":"2007/2010",
                "left":"room_2_16:room_5_32",
                "top":"",
                "right":"room_6_40:room_6_38",
                "bottom":"room_6_35"
            },
            {
                "id":"room_6_35",
                "title":"Konceptna tabla III ",
                "type":"E",
                "year":"",
                "left":"room_5_32",
                "top":"room_6_34",
                "right":"room_6_36",
                "bottom":"room_6_36"
            },
            {
                "id":"room_6_36",
                "title":"Rotas Axis Mundi",
                "type":"E",
                "year":"1995/1996",
                "left":"room_6_35",
                "top":"room_6_35",
                "right":"room_6_43",
                "bottom":"room_6_37"
            },
            {
                "id":"room_6_37",
                "title":"Rotas Sator v Equrni: Projekt Sreča Dragana",
                "type":"H",
                "year":"1997",
                "left":"room_5_31_2",
                "top":"room_6_35",
                "right":"",
                "bottom":"room_6_37"
            },
            {
                "id":"room_6_38",
                "title":"Konceptna tabla V",
                "type":"C+D",
                "year":"2006",
                "left":"room_6_34",
                "top":"room_6_37",
                "right":"room_7_46:room_7_45",
                "bottom":"room_6_39"
            },
            {
                "id":"room_6_39",
                "title":"Paradigma kontrole Arrive – Depart",
                "type":"A",
                "year":"2006",
                "left":"room_2_16",
                "top":"room_6_38",
                "right":"room_9_52_1",
                "bottom":"room_6_40"
            },
            {
                "id":"room_6_40",
                "title":"Paradigma Continuum-Entre-images",
                "type":"B",
                "year":"2014",
                "left":"room_6_34",
                "top":"room_6_39",
                "right":"room_6_42",
                "bottom":"room_6_42"
            },
            {
                "id":"room_6_42",
                "title":"Konceptna tabla XVI",
                "type":"B",
                "year":"",
                "left":"room_6_40",
                "top":"room_6_40",
                "right":"room_7_44",
                "bottom":"room_6_43"
            },
            {
                "id":"room_6_43",
                "title":"Konceptna tabla X",
                "type":"E",
                "year":"",
                "left":"room_6_36",
                "top":"room_6_42",
                "right":"room_9_51",
                "bottom":""
            },
        ],
        [
            {
                "id":"room_7_44",
                "title":"Metonomija zaznave",
                "type":"B",
                "year":"2007/2010",
                "left":"room_6_42",
                "top":"",
                "right":"",
                "bottom":"room_7_45"
            },
            {
                "id":"room_7_45",
                "title":"E-knjižni nomad",
                "type":"D",
                "year":"2010/2011",
                "left":"room_6_38",
                "top":"room_7_44",
                "right":"",
                "bottom":"room_7_46"
            },
            {
                "id":"room_7_46",
                "title":"Sonifikacija podobe",
                "type":"C",
                "year":"2014",
                "left":"room_6_38",
                "top":"room_7_45",
                "right":"room_7_47",
                "bottom":"room_7_47"
            },
            {
                "id":"room_7_47",
                "title":"Metamorfoza lingvistika",
                "type":"C",
                "year":"2007/2010",
                "left":"room_7_46",
                "top":"room_7_46",
                "right":"room_52_1",
                "bottom":"room_7_50"
            },
            {
                "id":"room_7_50",
                "title":"Konceptna tabla XII",
                "type":"G",
                "year":"2007/2010",
                "left":"room_4_25",
                "top":"room_7_47",
                "right":"",
                "bottom":""
            }
        ],
        [
            {
                "id":"room_9_51",
                "title":"Multimedijska konceptna tabla XVIII",
                "type":"E",
                "year":"",
                "left":"room_6_43",
                "top":"",
                "right":"",
                "bottom":"room_9_52_1"
            },
            {
                "id":"room_9_52_1",
                "title":"Sonifikacija podobe III",
                "type":"A+C",
                "year":"2016",
                "left":"room_6_39:room_7_47",
                "top":"room_9_51",
                "right":"room_9_52_2",
                "bottom":"room_9_52_2"
            },
            {
                "id":"room_9_52_2",
                "title":"Sonifikacija podobe III",
                "type":"A",
                "year":"2015",
                "left":"room_9_52_1",
                "top":"room_9_52_1",
                "right":"",
                "bottom":""
            }
        ]
    ]
}
