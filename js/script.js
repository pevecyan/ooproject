var avtor = "";
var naslovProjekta = "";
var projectTitle = "";
var gradivoProjekta = "";
var opisProjektaSlo = "";
var opisProjektaEng = "";
var lokacijaVidea = "";
var lokacijaSlike = "";
var lokacijaSlikeThumb = "";
var slikaSThumb = "";
var isMoving = false;
var isDragging = false;

$('.container').ready(function(){
    var startMousePos = {x:-1, y:-1};
    var currentContentPos = {x:0,y:0};
    var activeContentPos = {x:0,y:0};
    var mouseDown = false;

    $('.container').mousedown(function(event){
        return;
        startMousePos.x = event.pageX;
        startMousePos.y = event.pageY;
        mouseDown = true;
        console.log("down");
    });

    $(document).mouseup(function(){
        mouseDown = false;
        activeContentPos.x = currentContentPos.x;
        activeContentPos.y = currentContentPos.y;
        console.log("up");
        setTimeout(function(){
            isDragging = false;
        },500);
        
    });

    $('.container').mousemove(function(event) {
        if(mouseDown){
            isDragging = true;
            var currentMousePos = { x:  event.pageX, y: event.pageY };
            var width = $('.container').width();
            var height = $('.container').height();

            currentContentPos.x = activeContentPos.x + (currentMousePos.x-startMousePos.x);
            currentContentPos.y = activeContentPos.y + (currentMousePos.y-startMousePos.y);

            var contentDiv = $('.content');
            contentDiv.css("top", currentContentPos.y);
            contentDiv.css("left", currentContentPos.x);
            //console.log(parseInt(contentDiv.css("top"),10)+(currentMousePos.y-startMousePos.y));
        }
        //currentMousePos.x -= width/2;
        //currentMousePos.y -= height/2;

        //currentMousePos.y /= 100;
        //currentMousePos.x /= 100;

        //var contentDiv = $('.content');
        //contentDiv.css("top", parseInt(contentDiv.css("top"),10)-currentMousePos.y);
        //contentDiv.css("left", parseInt(contentDiv.css("left"),10)-currentMousePos.x);

        //console.log(currentMousePos);
    });

    $(".room").hover(function(e){
        //if(isMoving) return;
        var type = $(this).attr('type');
        if(e.type == "mouseenter"){
            
            if(type.indexOf('+')!= -1){
                $("[type*='"+type[0]+"']").addClass("markSameType");
                $("[type*='"+type[2]+"']").addClass("markSameType");
                
            }else{
                $("[type*='"+type+"']").addClass("markSameType");
            }
            
            $(this).removeClass("zoomOutRoomAnimation");
            $(this).children(".room-image").removeClass("hideRoomTitleAnimation");
            $(this).addClass("zoomInRoomAnimation");
            $(this).children(".room-image").addClass("showRoomTitleAnimation");
        }else{
            if(type.indexOf('+')!= -1){
                $("[type*='"+type[0]+"']").removeClass("markSameType");
                $("[type*='"+type[2]+"']").removeClass("markSameType");
                
            }else{
                $("[type*='"+type+"']").removeClass("markSameType");
            }
            $(this).removeClass("zoomInRoomAnimation");
            $(this).children(".room-image").removeClass("showRoomTitleAnimation");
            $(this).addClass("zoomOutRoomAnimation");
            $(this).children(".room-image").addClass("hideRoomTitleAnimation");
        }
        //console.log(this);
    })

    var nextClicked = false;

    $(".room-next").click(function(e){
        nextClicked = true;
        var destinationId = $(this).attr("data");

        var firstLeft = $("#room_1_1").offset().left;
        var firstTop = $("#room_1_1").offset().top;

        var left = $("#"+destinationId).offset().left;
        var top = $("#"+destinationId).offset().top;

        activeContentPos.x = (($(window).width()/2)-370) -(left-firstLeft);
        activeContentPos.y =  (($(window).height()/2)-200) -(top-firstTop);

        isMoving = true;
        $( ".content").animate({
            top: (($(window).height()/2)-200) -(top-firstTop),
            left: (($(window).width()/2)-370) -(left-firstLeft)
        }, 1000, function() {
            isMoving = false;
        });
    });

    $(".room").click(function(){
        if(isDragging) return;
        if(nextClicked)
        {
            nextClicked = false;
            return;
        }
        if(rooms[this.id].type =="video"){
            odpriModalVideo(rooms[this.id].data);
        }
        else if(rooms[this.id].type == "videoPhotoBackground"){
            odpriModalVideoPlusVideo(rooms[this.id].data);
        }
    });
});

function odpriModalVideo(data) {
    $.sweetModal({
        title: data.title,
        width: "80%",
         content: 
        '<div class="modalContainer" style="text-align: left;">'+
            '<div class="projectAuthor">'+data.avtor+'</div>'+
            '<div class="projectTitle">'+data.naslovProjekta+'</div>'+
            '<div class="projectSource">'+data.gradivoProjekta+'</div><hr/>' +
            '<div class="projectDescriptionSlo">'+data.opisProjektaSlo+'</div>' +
            '<br>'+
            '<div class="projectDescriptionEng" style="font-style: italic; color:gray">'+data.opisProjektaEng+'</div>'+
            '<br>'+
            '<p style="text-align:center">'+
                data.lokacijaVidea+
            '</p>'+
        '</div>'
    });
}
function odpriModalVideoPlusVideo(data) {
    $.sweetModal({
        title: data.title,
        width: "80%",
         content:
        '<div class="modalContainer" style="text-align: left; overflow:hidden">'+
            '<div class="projectAuthor">'+data.avtor+'</div>'+
            '<div class="projectTitle">'+data.naslovProjekta+'</div>'+
            '<div class="projectSource">'+data.gradivoProjekta+'</div><hr/>' +
            '<div class="projectDescriptionSlo">'+data.opisProjektaSlo+'</div>' +
            '<br>'+
            '<div class="projectDescriptionEng" style="font-style: italic; color:gray">'+data.opisProjektaEng+'</div>'+
            '<br>'+
            '<div class="container-all">'+
            '<div class="video-image-container">'+
                '<div class="'+data.klas+'" style=" margin-left:auto; margin-right:auto; background-image:url('+data.lokacijaSlike+');">'+
                    '<div class="'+data.klas2+'">'+
                        data.lokacijaVidea+
                    '</div>'+
                '</div>'+
            '</div></div>'+        
        '</div>'
    });
}