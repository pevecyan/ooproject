var rooms = require('./data.js');

var width = rooms.year.length;

var height = rooms.year[0].length;

rooms.year.forEach(function(element) {
    if(element.length > height){
        height = element.length;
    }
}, this);

//console.log(width + ":"+height);

var output=`
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
			
        <link href="css/style.css" rel="stylesheet">
        <link href="css/modal.css" rel="stylesheet">
        <link href="css/jquery.sweet-modal.min.css" rel="stylesheet">
		
        <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="js/script.js"></script>
        <script src="data/rooms.js"></script>
		<script src="js/lightbox.min.js"></script>
        <script src="js/jquery.sweet-modal.min.js"></script>
        
    </head>
    <body>
    <div class="title">
            <div>SREČO DRAGAN | Prostor, vržen iz tira | 22. december 2016 — 05. marec 2017 | Pregledna razstava | Muzej sodobne umetnosti Metelkova, +MSUM | Kustos: Igor Španjol </div>
		</div>
        <div class="container">
            
            <div class="content" style="left:0px;top:0px">
                <div class="row eras">
                    <div class="block first-era blank"> </div>
                    <div class="block era">
                        soba 1 – 60. leta
                    </div>
                    <div class="block era">
                        soba 2 – 70. leta
                    </div>
                    <div class="block era">
                        soba 3 – 80. leta
                    </div>
                    <div class="block era">
                        soba 4 – prehod  
                    </div>
                    <div class="block era">
                        soba 5 - 90. leta
                    </div>
                    <div class="block era">
                        soba 6 – 90. leta
                    </div>
                    <div class="block era">
                        soba 7 – 2000 
                    </div>
                    <div class="block era">
                        soba 9 – projekcije 
                    </div>
                </div>
`;

for(var y = 0; y < height; y++){
    var type = ""
    switch(y){
        case 0:
            type=`
            <div><b>O</b> - Nepovezani projekti</div>
            <div><b>A</b> - Izvajanje skripta</div>
            <div><b>A+B</b> - Alpha Theta ritem</div>
            <div><b>A+C</b> - Izvajanje skripta in analiza slike-videa</div>
            <div><b>B</b> - Artikulacija percepcije</div>
            <div><b>B+C</b> - Artikulacija percepcije in analiza slike</div>`;
            break;
        case 1:
            type=`
                <div><b>C</b> - Analiza slike</div>
                <div><b>D</b> - Nomad v omreženem mestu</div>
                <div><b>C+D</b> - Analiza slike-videa in hoja po omreženem mestu</div>
                <div><b>E</b> - Virtualni prostor</div>
                <div><b>F</b> - Analiza medija televizije</div>
                <div><b>G</b> - Remontaže</div>
                <div><b>H</b> - Oddaje o Draganovih razstavah, intervjuji</div>
            `;
            break;
       
    }
    output+= '\t\t\t\t<div class="row rooms">\n\t\t\t\t\t<div class="block first blank">'+type+'</div>';
    for(var x = 0; x < width; x++){
        if(rooms.year[x].length-1 < y){
            output+='\n\t\t\t\t\t<div class="block blank"></div>';
        }else{
            output+='\n\t\t\t\t\t<div class="block room" id="'+rooms.year[x][y].id+'" type="'+rooms.year[x][y].type+'">'
            output+='\n\t\t\t\t\t\t<div class="room-image"></div>';
            output+='\n\t\t\t\t\t\t<div class="room-content">'+rooms.year[x][y].title+'</div>';
            output+='\n\t\t\t\t\t\t<div class="room-type">'+rooms.year[x][y].type+'</div>';
            output+='\n\t\t\t\t\t\t<div class="room-year">'+rooms.year[x][y].year+'</div>';
            /*if(rooms.year[x][y].left != ""){
                if(rooms.year[x][y].left.split(":").length == 2){
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-left-top" data="'+rooms.year[x][y].left.split(":")[0]+'"></div>';
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-left-bottom" data="'+rooms.year[x][y].left.split(":")[1]+'"></div>';
                }else{
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-left" data="'+rooms.year[x][y].left+'"></div>';
                }
                
            }
            if(rooms.year[x][y].top != ""){
                output+='\n\t\t\t\t\t\t<div class="room-next room-next-top" data="'+rooms.year[x][y].top+'"></div>';
            }
            if(rooms.year[x][y].right != ""){
                if(rooms.year[x][y].right.split(":").length == 2){
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-right-top" data="'+rooms.year[x][y].right.split(":")[0]+'"></div>';
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-right-bottom" data="'+rooms.year[x][y].right.split(":")[1]+'"></div>';
                }else{
                    output+='\n\t\t\t\t\t\t<div class="room-next room-next-right" data="'+rooms.year[x][y].right+'"></div>';
                }
            }
            if(rooms.year[x][y].bottom != ""){
                output+='\n\t\t\t\t\t\t<div class="room-next room-next-bottom" data="'+rooms.year[x][y].bottom+'"></div>';
            }*/
            output+='\n\t\t\t\t\t</div>'
        }
    }
    output+='\n\t\t\t\t</div>'
}

output+=`\n\t\t\t\t
            </div>
        </div>
    </body>
</html>
`

console.log(output);