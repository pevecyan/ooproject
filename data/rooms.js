var rooms={
    "room_1_1":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Belo mleko belih prsi / White Milk of White Breasts (1969)",
            "projectTitle"      : "Belo mleko belih prsi (1969) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : "open-reel 2'', 30'",
            "opisProjektaSlo"   : "Statičen črno-bel posnetek »slušno govorne grupne komunikacije zlogov«, ki je obveljal za prvi video v jugoslovanskem prostoru.",
            "opisProjektaEng"   : "A static black-and-white recording of “auditory spoken group communication of syllables”, which is now considered the first video ever made in Yugoslavia.",
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/tpvheWbTIcs" frameborder="0" allowfullscreen></iframe>'
        }
    },
    "room_1_2_3":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "72 h zime in 72 h jeseni / In Winter 72 h and Autumn 72 h (1969/2016)",
            "projectTitle"      : "72 h zime in 72 h jeseni (1969/2016) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : "",
            "opisProjektaSlo"   : "Rekonstrukcija performansa 72 h zime 72 h jeseni in istoimenskega filma avtorjev Nuše & Sreča Dragana iz leta 1969 s 360-stopinjskim video posnetkom, ki si ga obiskovalec, ko vstavi pametni telefon v VR očala, lahko ogleda, kot bi bil prisoten na snemanju v parku Tivoli v Ljubljani. <br/>Happening sodi med land art projekte skupine OHO. Pri rekonstrukciji gre za avtorjev osebni prispevek k izumljanju metodologije arhiviranja na tehnologiji zasnovane, nestabilne elektronsko-digitalne umetniške produkcije. <br/>V projektu sta poleg avtorja sodelovala dr. Borut Batagelj iz Laboratorija za računalniški vid Fakultete za računalništvo in informatiko (pod vodstvom dr. Franca Soline) in programer Aljaž Romih.",
            "opisProjektaEng"   : "A reconstruction of the performance and film In Winter 72 h and Autumn 72 h from 1969 by Nuša & Srečo Dragan, with a 360-degree video recording that a visitor can view (by inserting a smart phone into a VR headset) as if he or she were present at the filming in Ljubljana’s Tivoli Park. <br/>The happening forms part of the land art projects of the OHO group. This reconstruction is the artist’s contribution to the development of a methodology for archiving technology-based, unstable electronic and digital artistic production. <br/>The other collaborators on the project were Borut Batagelj, PhD, from the Computer Vision Laboratory of the Faculty of Computer and Information Science (under the mentorship of Franc Solina, PhD) and programmer Aljaž Romih.",
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/sqqZc5bycKM?rel=0" frameborder="0" allowfullscreen></iframe>'
        }
    },
    "room_1_2_2":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "72 h zime in 72 h jeseni / In Winter 72 h and Autumn 72 h (1969)",
            "projectTitle"      : "72 h zime in 72 h jeseni (1969) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : "zvočni, čb / sound, bw, 8 mm, 2'05''",
            "opisProjektaSlo"   : '“Srečanje drug nasproti drugemu brez dotika z drevesom na sredi v parku Tivoli. <br/>Srečanje drug nasproti drugemu z dotikom dlani z drevesom na sredi v parku Tivoli. <br/>Srečanje drug nasproti drugemu z dotikom podplatov z drevesom na sredi v parku Tivoli.”',
            "opisProjektaEng"   : '“Coming together without touching with a tree in between in Tivoli Park. <br/>Coming together with hands touching with a tree in between in Tivoli Park. <br/>Coming together with soles touching with a tree in between in Tivoli Park.”',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/7i1q7r5Khtw?rel=0" frameborder="0" allowfullscreen></iframe>'
        }
    },
    "room_1_2_1":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "72 h zime in 72 h jeseni / In Winter 72 h and Autumn 72 h (1969)",
            "projectTitle"      : "72 h zime in 72 h jeseni (1969) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : "happening / happening",
            "opisProjektaSlo"   : '“Srečanje drug nasproti drugemu brez dotika z drevesom na sredi v parku Tivoli. <br/>Srečanje drug nasproti drugemu z dotikom dlani z drevesom na sredi v parku Tivoli. <br/>Srečanje drug nasproti drugemu z dotikom podplatov z drevesom na sredi v parku Tivoli.”<br><br><div class="projectDescriptionSubtext">Happening sodi med land art projekte skupine OHO.</div><br>',
            "opisProjektaEng"   : '“Coming together without touching with a tree in between in Tivoli Park. <br/>Coming together with hands touching with a tree in between in Tivoli Park. <br/>Coming together with soles touching with a tree in between in Tivoli Park.”<br><br><div class="projectDescriptionSubtext">The happening was part of the land art projects of the OHO group.</div>',
            "lokacijaVidea"     : ''
        }
    },
    "room_1_7":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla VII / Concept Board 7 (2010)",
            "projectTitle"      : "Konceptna tabla VII / Concept Board 7 (2010) - Srečo Dragan",
            "gradivoProjekta"   : "",
            "opisProjektaSlo"   : 'Gibljive slike, ki se kontekstualizirajo kot perceptivne in programirane situacije performansa v mediju filma (the expanded cinema) in OHO-jevih Poletnih projektih (land art).'+
                                    '<br>Od leve proti desni:<br>1. koncept, fotografije in filmski skript – risbe pozicij premikanja za film<br>2. prikazuje se film 72 ur zime, 72 ur jeseni, park Tivoli, Ljubljana, 1969<br>3. koncept in fotodokumentacija projekta 16 m2 ali 40 x 50 cm peska pod steklom na Zemlji 21. junija 1969 ob 15. uri pred Moderno galerijo v Ljubljani.',
            "opisProjektaEng"   : 'Moving pictures contextualized as perceptive and programmed situations of performance in film (the expanded cinema) and Summer Projects by OHO (land art).'+
                            '<br>From left to right:<br>1. concept, photographs and film script – sketches for a storyboard<br>2. screening of the film 72 Hours of Winter, 72 Hours of Autumn, Tivoli Park, Ljubljana, 1969<br>3. concept and photo-documentation of the project 16 m2 or 80 x 40 x 50 cm of Gravel Under Glass on Earth, 21st June 1969, at 3 p.m., in front of the Museum of Modem Art in Ljubljana.',
            "lokacijaVidea"     : '<iframe width="417" height="315" src="https://www.youtube.com/embed/7i1q7r5Khtw" frameborder="0" allowfullscreen></iframe>',
            "lokacijaSlike"     : 'img/1-7__tabla4sd1-popravljenaTONIRANA-1.png',
            "klas"              : 'video-image17',
            "klas2"             : 'video-image-video17'
        }
    },
    "room_1_8":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla VI / Concept Board 6 (2010)",
            "projectTitle"      : "Konceptna tabla VI / Concept Board 6 (2010) - Srečo Dragan",
            "gradivoProjekta"   : "",
            "opisProjektaSlo"   : 'Konceptualna umetnost, artikulirana v medijih teksta, fotografije, filma, happeninga in odtisa v reviji Ekran.'+
                            '<br>Od leve proti desni:<br>1. fotografija iz videa Belo mleko belih prsi, čb, zvok, 1969<br>2. fotografija happeninga Različne stvari se razstavljajo, Titova cesta, Ljubljana, 1969<br>3. fotografija projekta Stonoga, park Zvezda, Ljubljana, 1968<br>4. prikazuje se film RG, čb, zvok, 1969.',
            "opisProjektaEng"   : 'Conceptual art articulated as text, photography, film, happening, and print in Ekran magazine.'+
                            '<br>From left to right:<br>1. photograph from the video The White Milk of White Breasts, bw, sound, 1969<br>2. photograph of the happening Different Things Are Exhibited, Titova Street, Ljubljana, 1969<br>3. photograph of the Centipede project, Zvezda Park, Ljubljana, 1968<br>4. screening of RG film, bw, sound, 1969.',
            "lokacijaVidea"     : '<iframe width="417" height="315" src="https://www.youtube.com/embed/PI7i4bO3xfA" frameborder="0" allowfullscreen></iframe>',
            "lokacijaSlike"     : 'img/1-8__tabla1sd_2-popravljena-1.png',
            "klas"              : 'video-image18',
            "klas2"              : 'video-image-video18'
        }
    },
    "room_2_3_1":{
        "type":"video",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "40 x 50 x 80 cm peska pod steklom / 40 x 50 x 80 cm of Gravel under Glass (1969)",
            "projectTitle"      : "40 x 50 x 80 cm peska pod steklom / 40 x 50 x 80 cm of Gravel under Glass (1969) - Srečo Dragan",
            "gradivoProjekta"   : "happening / happening",
            "opisProjektaSlo"   : 'Projekt je bil izveden pred stavbo Moderne galerije na dan, ko je človek prvič pristal na Luni. Postopek pogojuje velikost stekla. Avtor je steklo položil na tla, v njem poiskal svoj odsev in ga nato obrisal.',
            "opisProjektaEng"   : 'The project was carried out in front of the Moderna galerija building on the day of the first manned Moon landing. The procedure was dictated by the size of the glass pane. The artist placed a glass pane of the ground, sought out his reflection in it and outlined it.',
            "lokacijaVidea"     : '',
        }
    },
    "room_2_3_2":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Wizard (1970)",
            "projectTitle"      : "Wizard (1970) - Srečo Dragan",
            "gradivoProjekta"   : "odtis / stencil",
            "opisProjektaSlo"   : 'Projekt vizualne poezije, kjer izbrane črke (4., 9. in 1.) na simetričnih mestih glede na začetek in konec abecede sestavljajo magično besedo.',
            "opisProjektaEng"   : 'Visual poetry, in which letters that are selected (4th, 9th, 1st) from the symmetrical positions from the beginning and the end of the alphabet form the word wizard.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/2-17__07-correlation-square-selection-1.jpg',
            "klas"              : 'video-image232',
            "klas2"              : ''
        }
    },
    "room_2_9":{
        "type":"video",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "40 x 50 x 80 cm peska pod steklom / 40 x 50 x 80 cm of Gravel under Glass (1969)",
            "projectTitle"      : "40 x 50 x 80 cm peska pod steklom / 40 x 50 x 80 cm of Gravel under Glass (1969) - Srečo Dragan",
            "gradivoProjekta"   : "happening / happening",
            "opisProjektaSlo"   : 'Projekt je bil izveden pred stavbo Moderne galerije na dan, ko je človek prvič pristal na Luni. Postopek pogojuje velikost stekla. Avtor je steklo položil na tla, v njem poiskal svoj odsev in ga nato obrisal.',
            "opisProjektaEng"   : 'The project was carried out in front of the Moderna galerija building on the day of the first manned Moon landing. The procedure was dictated by the size of the glass pane. The artist placed a glass pane of the ground, sought out his reflection in it and outlined it.',
            "lokacijaVidea"     : '',
        }
    },
    "room_2_10":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Miselna vizualizacija objekta / The Thinking Visualization of an Object (1972)",
            "projectTitle"      : "Miselna vizualizacija objekta / The Thinking Visualization of an Object (1972) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'Offset<br><p style="margin-top: -0.5%; font-size:90%;">1. aprilski susreti, SKC Beograd</p>',
            "opisProjektaSlo"   : '“Zapiši, kako si mislil videno kocko ob prvem pogledu / l - levo, r - desno / napisano je spomenik miselne vizualizacije.”',
            "opisProjektaEng"   : '“Write how you thought the cube at first glance – left or right. Writing is a memorial of thinking visualization.”',
            "lokacijaVidea"     : '',
        }
    },
    "room_2_11":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Smer relacije senzibilnost-imaginacija / The Relationship of Sensibility and Imagination (1972)",
            "projectTitle"      : "Smer relacije senzibilnost-imaginacija / The Relationship of Sensibility and Imagination (1972) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'Offset<br><p style="margin-top: -0.5%; font-size:90%;">1. aprilski susreti, SKC Beograd</p>',
            "opisProjektaSlo"   : '“Nariši 8 linij, paralelnih med sabo, kot ti veleva imaginacija, in napiši ime tvojega očeta in sina.”',
            "opisProjektaEng"   : '“Draw 8 lines which are parallel in your imagination. Write your father’s and son’s name.”',
            "lokacijaVidea"     : '',
        }
    },
    "room_2_15":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla IX / Concept Board 9 (1974)",
            "projectTitle"      : "Konceptna tabla IX / Concept Board 9 (1974) - Srečo Dragan",
            "gradivoProjekta"   : '2. rekonceptualizacija',
            "opisProjektaSlo"   : 'Projekt je bil posebej realiziran za 3. aprilski susret proširenih medija v SKC leta 1974 v Beogradu.<br>V delu se je zgodil premik od statičnega konceptualizma k spiritualni izkušnji, ki se je v mediju okrogle mize kot strnjenem izjavljanju gestualnih sporočil zarisovala v umetnikovo in gledalčevo zavest. Pri tem je alfa ritem ponavljajoč in theta ustvarjalen.<br>Projekt je trajal sedem dni in noči. V njem so aktivno sodelovali Joseph Beuys, Ješa Denegri, Braco Dimitrijević, Bogdanka in Dejan Poznanović, Giancarlo Politi, avtorja Nuša in Srečo Dragan ter številni drugi udeleženci.',
            "opisProjektaEng"   : 'The project was realized specially for the 3rd April Meeting of Expanded Media at the Students’ Cultural Center in Belgrade in 1974.<br>This work saw a shift from static conceptual art to a spiritual experience, which was imprinted on the artist’s and the viewers’ consciousness at the panel discussion as an uninterrupted body of communication comprised of gestural messages. Here, the alpha rhythm is repetitive, and the theta rhythm creative.<br>The project lasted seven days and nights. Among its active participants were Joseph Beuys, Ješa Denegri, Braco Dimitrijević, Bogdanka and Dejan Poznanović, Giancarlo Politi, Nuša and Srečo Dragan, and many others.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/2-15-08_Beuys-1.jpg',
            "klas"              : 'video-image215',
            "klas2"              : ''
        }
    },
    "room_2_16":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Alpha Theta Ritem / Alpha Theta Rhythm (1974)",
            "projectTitle"      : "Alpha Theta Ritem / Alpha Theta Rhythm (1974) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'otografije / photos<br><p style="margin-top: -0.5%; font-size:90%;">Moderna galerija, Ljubljana</p>',
            "opisProjektaSlo"   : 'Projekt temelji na vzporednicah med konceptualizmom in arhaičnimi ljudskimi tradicijami. Pri konceptualnem delu gre za to, da umetnik uporabi materialni predmet ali model, da občinstvu posreduje svojo misel.<br>Problematičen je ravno proces prenašanja miselnega modela v misel drugega. Podobno so v tradicionalnem ustnem izročilu mitološke, zgodovinske in druge simbolne strukture prehajale od enega pripovedovalca do drugega, se tako ohranjale, a tudi nujno spreminjale.<br>En del akcije (tu je sodeloval tudi Joseph Beuys) sta Draganova zasnovala kot kroženje izkušenj v komunikaciji med sodelavci, drugi del pa kot prenašanje giba, ki so ga udeleženci ponovili in prenesli drugemu.',
            "opisProjektaEng"   : 'The project was based on parallels between conceptualism and archaic folk traditions. In conceptualism, an artist uses a material object or model to communicate his or her idea to the audience.<br>This process of transporting a thought model into the thoughts of others is essential. Similarly, in oral traditions, mythological, historical or other symbolic structures travelled from one narrator to another. In this way they were preserved, but also transformed.<br>One part of their action (which also involved Joseph Beuys) was based on the circulation of communication experiences between collaborating artists, whereas the other consisted of the transfer of movement repeated and passed on by the participants.',
            "lokacijaVidea"     : '',
        }
    },
    "room_3_18":{
        "type":"video",
        "data":{
            "avtor"             : "Avtor: Nebojša Đukelić",
            "naslovProjekta"    : "Pokretne slike / Moving Pictures (1981)",
            "projectTitle"      : "Pokretne slike / Moving Pictures (1981) - Nebojša Đukelić",
            "gradivoProjekta"   : 'televizijska oddaja / television show<br><p style="margin-top: -0.5%; font-size:90%;">TV Beograd</p>',
            "opisProjektaSlo"   : 'Televizijski program je bil pomemben medij produkcije, reprezentacije in refleksije avtorskega videa. V osemdesetih letih se je odnos med avtorskim videom in televizijo razvijal tako, da video ustvarjalci v televiziji niso več nujno videli velikega nasprotnika, kot je to bilo v pionirskih sedemdesetih letih, ko se je video vzpostavil kot reakcija na naraščajočo moč televizije.<br>Nuša in Srečo Dragan sta televizijske oddaje uporabljala za prizorišča svojih akcij, televizijske hiše v Ljubljani, Beogradu in Sarajevu pa vse do leta 1987 kot producente svojih videov.<br>V oddaji Pokretne slike znotraj filmskega programa TV Beograd sta izvedla petminutni Video šus, ki je nazorno ilustriral osrednjo temo oddaje – avtorsko dekonstrukcijo televizijske slike.',
            "opisProjektaEng"   : 'For video art, television programs were important as a medium of production, presentation, and reflection. In the 1980s, the relationship between video art and television changed: video artists no longer saw television as their greatest obstacle or opponent as they had in the pioneering 1970s, when video evolved as a reaction to the growing power of television.<br>Nuša and Srečo Dragan realized a project on television as early as the late 1970s (Eastbound from the East, 1978), subsequently using TV programs as scenes of their actions, and the Ljubljana, Belgrade, and Sarajevo television stations as the producers of their videos until 1987.<br>They performed their five-minute action Video Shoes (1981) on the TV Belgrade program Pokretne slike (Moving Pictures) about film, in which the action illustrated the main theme of the program – the artistic deconstruction of the televised picture.',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/0OSsrQhQQXQ?rel=0" frameborder="0" allowfullscreen></iframe><br><iframe width="560" height="315" src="https://www.youtube.com/embed/kARB3WJRFZE" frameborder="0" allowfullscreen></iframe><br><iframe width="560" height="315" src="https://www.youtube.com/embed/lBwdjNTvErA" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_3_21":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Spomenik žrtev in orgije: padec človeštva na odeških stopnicah / Monument of Sacrifice and Orgy: the Fall of Humankind on the Odessa Steps (1985)",
            "projectTitle"      : "Spomenik žrtev in orgije: padec človeštva na odeških stopnicah / Monument of Sacrifice and Orgy: the Fall of Humankind on the Odessa Steps (1985) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'video instalacija, 5 TV ekranov, lesene stopnice, lestev, risba na platnu / video installation, 5 TV screens, wooden stairs, ladder, drawing on canvas',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/0s9eSF13Dn0?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_3_22":{
        "type":"video",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Video anamorfoza I, IX, III / Video Anamorphosis 1, 9, 3, (1987)",
            "projectTitle"      : "Video anamorfoza I, IX, III / Video Anamorphosis 1, 9, 3, (1987) - Srečo Dragan",
            "gradivoProjekta"   : 'risba na tisk / drawing, prints',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
        }
    },
    "room_3_23":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Spomenik žrtev in orgije / Monument of Sacrifice and Orgy (1985)",
            "projectTitle"      : "Spomenik žrtev in orgije / Monument of Sacrifice and Orgy (1985) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'U-matic high band, 8"',
            "opisProjektaSlo"   : 'Prizori iz Eisensteinove Križarke Potemkin so poudarjeni z glasbo Einstürzende Neubauten. Citat zaključi grafični znak, performans pa postane del video površine.',
            "opisProjektaEng"   : 'The work presents scenes from Eisenstein’s Battleship Potemkin, supported by Einstürzende Neubauten. A quotation is added to the graphic sign, and the performance becomes part of the video surface. ',
            "lokacijaVidea"     : '',
        }
    },
    "room_3_24":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Meščevo zlato / The Light of a Golden Moon (1987)",
            "projectTitle"      : "Meščevo zlato / The Light of a Golden Moon (1987) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'beta SP, U-matic high band, 14"',
            "opisProjektaSlo"   : 'Uporaba odlomkov iz Resnaisovega filma Lani v Marienbadu pokaže glavno določilo videa: inkrustacijo slike z drugo sliko. Avtorja se pojavljata v prizorih iz filma, pri čemer se njuni figuri umeščata v fiktivni prostor.',
            "opisProjektaEng"   : 'The use of fragments from Resnais’s film Last Year in Marienbad represents the main determinant of the video image – inlaying a picture in another picture. The artists appear in scenes from the film, in which their figures are incorporated into fictional space.',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/PCxGNwYhmPU?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_4_25":{
        "type":"video",
        "data":{
            "avtor"             : "Avtorja: Nuša & Srečo Dragan",
            "naslovProjekta"    : "Gosposvetski sen / The Daydream of Gospasveta Field (1987)",
            "projectTitle"      : "Gosposvetski sen / The Daydream of Gospasveta Field (1987) - Nuša & Srečo Dragan",
            "gradivoProjekta"   : 'U-matic high band, 9"',
            "opisProjektaSlo"   : 'Remontaža slike v sliki se metamorfično spreminja in vključuje prizor iz filma Nostalgija Andreja Tarkovskega, ki mu je video posvečen. Nuša in Srečo Dragan sta skupaj posnela več avtorskih videov, v katerih sta sama scenarista in igralca, v poznejših delih pa uporabljata tudi citate iz zgodovine filmske umetnosti (Eisenstein, Tarkovski, Resnais).',
            "opisProjektaEng"   : 'The image-in-image in this video undergoes a metamorphosis, which includes a scene from the film Nostalgia by Andrei Tarkovsky, to whom this video is dedicated. Together, Nuša and Srečo Dragan made several artistic videos that they also wrote and performed in, and included quotes from the history of film (Eisenstein, Tarkovsky, Resnais) in their later works.',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/hF3KnTi-QE8?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_4_26":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla XI / Concept Board 11",
            "projectTitle"      : "Konceptna tabla XI / Concept Board 11 - Srečo Dragan",
            "gradivoProjekta"   : '',
            "opisProjektaSlo"   : 'Voajer, ekshibicionist, narcis, od osebnega k javnemu, prvi vstop v umetnost 80-ih, video dela na javni TV.<br>Od leve proti desni:<br>Nuša & Srečo Dragan, Image no Door, 4", 35-milimetrska delavnica, RTV Slovenija, 1982,<br>Nuša & Srečo Dragan, Video šus, 3", Pokretne slike, RTV Beograd, 1982,<br>Nuša & Srečo Dragan, Spomenik žrtev in orgije, 7", oddaja Dobre vibracije, TV Sarajevo, 1985.',
            "opisProjektaEng"   : 'The image-in-image in this video undergoes a metamorphosis, which includes a scene from the film Nostalgia by Andrei Tarkovsky, to whom this video is dedicated. Together, Nuša and Srečo Dragan made several artistic videos that they also wrote and performed in, and included quotes from the history of film (Eisenstein, Tarkovsky, Resnais) in their later works.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/4-26__04_PAROLE_2avenirPOPnov.png',
            "klas"              : 'video-image426',
            "klas2"              : ''
        }
    },
    "room_5_28":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Dragan Srečo',
            "naslovProjekta"    : "Madež / Macule, (1997)",
            "projectTitle"      : "Madež / Macule, (1997) - Srečo Dragan",
            "gradivoProjekta"   : 'U-matic, 15"',
            "opisProjektaSlo"   : 'Video se navezuje na sliko Sejalec Ivana Groharja.',
            "opisProjektaEng"   : 'This video is based on the painting The Sower by Ivan Grohar.',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/k-osEsPJHVg?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_5_29":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Dragan Srečo',
            "naslovProjekta"    : "Opus Canum (1997)",
            "projectTitle"      : "Opus Canum (1997) - Srečo Dragan",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation',
            "opisProjektaSlo"   : 'Projekt digitalnega slikanja, kjer je obiskovalec na ekranih na dotik s prstom risal po sliki in jo spreminjal.',
            "opisProjektaEng"   : 'Opus Canum is a digital painting project in which the user paints with his finger over the image on the touch-screens.',
            "lokacijaVidea"     : '',
        }
    },
    "room_5_30":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla XV / Concept Board 15",
            "projectTitle"      : "Konceptna tabla XV / Concept Board 15 - Srečo Dragan",
            "gradivoProjekta"   : 'Tri paradigme. / Three Paradigms.',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/5-30__06-threeparadigms_ogled-OUTLINES.png',
            "klas"              : 'video-image530',
            "klas2"              : ''
        }
    },
    "room_5_31_1":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtorja: Narvika Bovcon & Aleš Vaupotič',
            "naslovProjekta"    : "Kaj je videoart? Intervju s Srečom Draganom / What is video art? Interview with Srečo Dragan (2002)",
            "projectTitle"      : "Kaj je videoart? Intervju s Srečom Draganom / What is video art? Interview with Srečo Dragan (2002) - Narvika Bovcon & Aleš Vaupotič",
            "gradivoProjekta"   : '15’, 24’’, TV Slovenija, 2002 video intervju / video intervieww<br><p style="margin-top: -0.5%; font-size:90%;">15’, 24’’, TV Slovenija, 2002</p>',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/NZ6NZaq9a3s?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_5_31_2":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtorja: Narvika Bovcon & Aleš Vaupotič',
            "naslovProjekta"    : "Video-integrirani mediji. Srečo Dragan: Rotas - Sator (1993-98), Čas, vržen iz tira (2000-) / Video-integrated media. Srečo Dragan: Rotas - Sator (1993-98), Time is Out of Joint (2000-) (2002)",
            "projectTitle"      : "Video-integrirani mediji. Srečo Dragan: Rotas - Sator (1993-98), Čas, vržen iz tira (2000-) / Video-integrated media. Srečo Dragan: Rotas - Sator (1993-98), Time is Out of Joint (2000-) (2002) - Narvika Bovcon & Aleš Vaupotič",
            "gradivoProjekta"   : '20’, 15’’, TV Slovenija, 2002 video intervju / video intervieww<br><p style="margin-top: -0.5%; font-size:90%;">20’, 15’’, TV Slovenija, 2002</p>',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/NZ6NZaq9a3s?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_5_32":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla XV / Concept Board 15",
            "projectTitle"      : "Konceptna tabla XV / Concept Board 15 - Srečo Dragan",
            "gradivoProjekta"   : 'Tri paradigme. / Three Paradigms.',
            "opisProjektaSlo"   : 'Video instalacija kot instrumentalizacija medija videa in njegova povezava s klasičnimi umetniškimi mediji v novo kontekstualno povezavo oziroma generirano podobo.<br>Od leve proti desni:<br>1. reprodukcija ohridske ikone Marijinega oznanjenja, okoli 1300,<br>2. NEO GEO, video, barvni, zvočni, 5’, 1989,<br>3. NEO GEO VII in NEO GEO VIII, video instalaciji, ekrani, les, kovina, barva, 1989.',
            "opisProjektaEng"   : 'Video installation as the instrumentalization of video as a medium and its connecting with classic art media into a new contextual whole – a generated image.<br>From left to right:<br>1. reproduction of the Ohrid icon of the Annunciation, around 1300,<br>2. video NEO GEO, color, sound, 5’, 1989,<br>3. video installation NE0 GEO 7 and NE0 GEO 8, screens, wood, metal, paint, 1989.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/5-32__02_revertedperspective_2avenir-POP-nov.png',
            "klas"              : 'video-image532',
            "klas2"              : ''
        }
    },
    "room_5_33":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Srečo Dragan',
            "naslovProjekta"    : "Arheus (Piero della Francesca in virtualna umetnost) / Archeus (Piero della Francesca and Virtual Art) (1992-93)",
            "projectTitle"      : "Arheus (Piero della Francesca in virtualna umetnost) / Archeus (Piero della Francesca and Virtual Art) (1992-93) - Srečo Dragan",
            "gradivoProjekta"   : 'digitalni video / digital video, 7’ 45’’',
            "opisProjektaSlo"   : 'Umetnik izpostavi delo Prospettiva di città renesančnega umetnika Piera della Francesce in virtualno resničnost lika Arheusa. Oba presegata referenčni okvir gledalčeve izkušnje. Fiktivni prostor dodatno ustvarja Laibach s svojo verzijo italijanske renesančne glasbe.',
            "opisProjektaEng"   : 'The artist considers the Renaissance painting Prospettiva di città by Piero della Francesca and the virtual reality of the figure of Archeus. Both are beyond the frame of reference of the observer’s experience. The sound of Laibach, with their version of Italian Renaissance music, further constructs the fiction of space. ',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/pqx_VH-r8As?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_6_34":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Silvo Gazvoda',
            "naslovProjekta"    : "Metaforične razširitve / Metaphorical Expansions (2007/2010)",
            "projectTitle"      : "Metaforične razširitve / Metaphorical Expansions (2007/2010) - Silvo Gazvoda",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation ',
            "opisProjektaSlo"   : 'V projektu je uporabnik povabljen k ubesedenju relacij, konfliktnih ali pa razrešitvenih, med dvema izbranima izrezoma dogajanja na podobah z mitološkim ozadjem. Namesto video kamere na stativu, kot v primeru zapisa skupinske komunikacije v mediju videa, se uporabnikova prisotnost zajame s tehnologijo računalniškega vida, ki prepozna uporabnikov obraz, ga kadrira, zajame in obenem posname izjavljanje, vse skupaj pa shrani v podatkovno zbirko, iz katere se izjave različnih udeležencev kasneje predvajajo. Programer: Silvo Gazvoda.',
            "opisProjektaEng"   : 'In this project, the user is invited to verbalize relations (of conflict or resolution) between two chosen segments of scenes that are grounded in mythology. Rather than using a video camera on a tripod (like recording group communication on video), the user’s presence is captured with computer vision technology, which recognizes the user’s face, and which frames and captures it while recording his or her statement at the same time. The whole is stored in a database, from which the statements of various participants are subsequently played. Programmer: Silvo Gazvoda.',
            "lokacijaVidea"     : '',
        }
    },
    "room_6_35":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla III / Concept Board 3",
            "projectTitle"      : "Konceptna tabla III / Concept Board 3 - Srečo Dragan",
            "gradivoProjekta"   : '',
            "opisProjektaSlo"   : 'Digitalni video kot primer spremembe rakurzov kamere v trajektorijo poti. Zamenjavo srečamo že v filmih Dziga Vertova, ki je vrgel vključeno kamero v zrak, da je snemala pogled pred sabo med letom.<br>Od leve proti desni:<br>1. Dziga Vertov, Mož s kamero, 1927,<br>2 (36). Srečo Dragan, Rotas Axis Mundi, računalniški 3D video 5’, produkcija TV Slovenija, Axel Tribe, Emotion film, digitalna montaža VPK, 1995/96,<br>3 (28). Srečo Dragan, Madež, digitalni video 31’, TV Slovenija, 1991,<br>4 (25). Nuša & Srečo Dragan, Gosposvetski sen, video, 8’, TV Sarajevo, 1988.',
            "opisProjektaEng"   : 'Digital video as an example of swapping camera angles for a trajectory. This can already be found in the films of Dziga Vertov, who threw an operating camera into the air so that it recorded the view ahead of it on its trajectory.<br>From left to right:<br>1. Dziga Vertov, The Man with the Movie Camera, 1927,<br>2. Srečo Dragan, Rotas Axis Mundi, computer 3-D video, 5’, production TV Slovenia, Axel Tribe, Emotion film, digital editing VPK, 1995/96,<br>3. Srečo Dragan, Macule, digital video, 31’, TV Slovenia, 1991,<br>4. Nuša & Srečo Dragan, The Daydream of Gospasveta Field, video, 8’, TV Sarajevo, 1988.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/6-35__01_navigation_2avenirPOP-nov.png',
            "klas"              : 'video-image635',
            "klas2"              : ''
        }
    },
    "room_6_36":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Srečo Dragan',
            "naslovProjekta"    : "Rotas Axis Mundi (1995/1996)",
            "projectTitle"      : "Rotas Axis Mundi (1995/1996) - Srečo Dragan",
            "gradivoProjekta"   : 'računalniški 3-D video / computer 3-D video, 4’45’’',
            "opisProjektaSlo"   : 'Norost modernega sveta je bila zasnovana že na začetku in Babilonski stolp je njena prispodoba: od Babilonskega stolpa do Sarajeva, od imaginarnih arhitektur do nerealiziranih načrtov arhitekta Jožeta Plečnika.<br>V videu se prostor, ki ga predstavlja dvodimenzionalna slika, prepleta s prostori računalniške animacije: prostor podobe nasproti digitalnim prostorom, ki se neprestano transformirajo, segajo v različne časovne perspektive.<br>Podobe iz filma Metropolis Fritza Langa so bile vizija prihodnosti, tu pa nenadoma postanejo podobe zunaj vsakega časa in prostora.<br>Produkcija: TV Slovenia, Arxel Tribe & E-Motion film, Ljubljana.<br>Music: Laibach, Kraftwerk, Johann Strauss II.<br>Digitalna montaža: VPK.',
            "opisProjektaEng"   : 'The insanity of the modern world is inherent from the very beginning and the Babylon Tower is its symbol: from the Babylon Tower to Sarajevo, from imaginary architectures to the unrealised designs by architect Joze Plecnik.<br>In this video, the space represented by the two-dimensional image intertwines with the spaces of computer animation: the space of an image confronting digital spaces which are constantly transforming, reaching into different time perspectives.<br>The images from Fritz Lang’s film Metropolis were a vision of the future, but here they suddenly become images outside of time and space.<br>Production: TV Slovenia, Arxel Tribe & E-Motion film, Ljubljana.<br>Music: Laibach, Kraftwerk, Johann Strauss II.<br>Digital editing: VPK.',
            "lokacijaVidea"     : '<iframe width="560" height="315" src="https://www.youtube.com/embed/K9v3PFRWH1o?rel=0" frameborder="0" allowfullscreen></iframe>',
        }
    },
    "room_6_37":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Dominik Križan',
            "naslovProjekta"    : "Rotas Sator v Equrni: Projekt Sreča Dragana / Rotas Sator in Equrna: A Project by Srečo Dragan (1997)",
            "projectTitle"      : "Rotas Sator v Equrni: Projekt Sreča Dragana / Rotas Sator in Equrna: A Project by Srečo Dragan (1997) - Dominik Križan",
            "gradivoProjekta"   : 'računalniški 3-D video / computer 3-D video, 4’45’’<br><p style="margin-top: -0.5%; font-size:90%;">TV Slovenija, 20. 1. 1997</p>',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
        }
    },
    "room_6_38":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla V / Concept Board 5",
            "projectTitle"      : "Konceptna tabla V / Concept Board 5 - Srečo Dragan",
            "gradivoProjekta"   : '',
            "opisProjektaSlo"   : 'Razširitev novomedijskega projekta v polje mobilne instalacije kot uprizarjanje umetnosti v javnih prostorih digitalne družbe.<br>Galerija kot laboratorij za kodiranje hoje in podatkovni center.<br>Predstavitev projekta Matrica koincidenca v galeriji Spomeniškovarstvenega centra, Ljubljana, 2006<br>Postavitev v galeriji Instituta Jožef Stefan, Ljubljana, 2007<br>Razširjena postavitev na Trienalu sodobne umetnosti v Sloveniji U3, Moderna galerija, 2010',
            "opisProjektaEng"   : 'Expanding a new media project into the field of a mobile installation as a way to perform art in the public space of digital society.<br>A gallery as a laboratory for walk-coding and a database center.<br>Presenting the Matrix Coincidence project at the gallery of Spomeniškovarstveni center, Ljubljana, 2006<br>An installation at the Jožef Stefan Institute Gallery, Ljubljana, 2007<br>An extended installation at the Triennial of Contemporary Art in Slovenia U3, Museum of Modern Art, Ljubljana, 2010',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/6-38__DraganKatalog2006POP-crop.png',
            "klas"              : 'video-image638',
            "klas2"              : ''
        }
    },
    "room_6_39":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Srečo Dragan',
            "naslovProjekta"    : "Paradigma kontrole Arrive – Depart / Paradigm of Arrive – Depart Control (2014)",
            "projectTitle"      : "Paradigma kontrole Arrive – Depart / Paradigm of Arrive – Depart Control (2014) - Srečo Dragan",
            "gradivoProjekta"   : 'konceptna video projekcija / concept video screening',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
        }
    },
    "room_6_40":{
        "type":"video",
        "data":{
            "avtor"             : 'Avtor: Srečo Dragan',
            "naslovProjekta"    : "Paradigma Continuum-Entre-images / Paradigm of Continuum-Entre-images (2014)",
            "projectTitle"      : "Paradigma Continuum-Entre-images / Paradigm of Continuum-Entre-images (2014) - Srečo Dragan",
            "gradivoProjekta"   : 'konceptna video projekcija / concept video screening, 4’ 40’’',
            "opisProjektaSlo"   : 'Delo raziskuje točko presečišča v teorijah Raymonda Bellourja, Michela Foucaulta, Clauda Lévi-Straussa, Rolanda Barthesa in Gillesa Deleuzea, ki govore o fotografiji, filmu in videu.<br>Avtor izpostavlja odločilni akt video projekcije, v katerem sodelujejo delo (z vso submedialno plastjo), gledalec, ki je v pogledu suspenza, in tretji, ki ta dogodek presenečenja spremlja in je zmožen videti »med« prostore celotnega dogajanja.<br>Njegova pozicija je v stalnem premikanju in je opazovalec ne more razumsko locirati.',
            "opisProjektaEng"   : 'The work explores the point of intersection in the theories of Raymond Bellour, Michel Foucault, Claude Lévi-Strauss, Roland Barthes, and Gilles Deleuze on photography, film, and video.<br>The artist puts particular emphasis on the decisive act of video projection, which involves the work (with its sub-media layer), the viewer who witnesses the suspense, and a third party who watches this surprise event and is in a position to see “between” the spaces of the action.<br>This third party is in constant motion, so his/her position cannot be rationally determined by the observer.',
                "lokacijaVidea"     : '',
        }
    },
    "room_6_42":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla XVI / Concept Board 16",
            "projectTitle"      : "Konceptna tabla XVI / Concept Board 16 - Srečo Dragan",
            "gradivoProjekta"   : 'Matrix',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/6-42_subthematic-standards2.png',
            "klas"              : 'video-image642',
            "klas2"              : ''
        }
    },
    "room_6_43":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla X / Concept Board 10",
            "projectTitle"      : "Konceptna tabla X / Concept Board 10 - Srečo Dragan",
            "gradivoProjekta"   : 'Vzporedni prostori / Parallel Spaces',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/6-43__03_parallelspaces_2avenirPOPnovOUTLINES.png',
            "klas"              : 'video-image643',
            "klas2"              : ''
        }
    },
    "room_7_44":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "Metonomija zaznave / Perceptions Metonomy (2007/2010)",
            "projectTitle"      : "Metonomija zaznave / Perceptions Metonomy (2007/2010)",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation',
            "opisProjektaSlo"   : 'Obiskovalec v procesu okušanja hrane izbira eno od dvojic sladko-slano, kislo-slano, pekoče-slano in grenko-slano. Okus posamezne jedi nato poimenuje z dvema besedama.<br>Udeležba v projektu obiskovalcu omogoča, da preko kognitivne obdelave okusov in osebne jezikovne interpretacije vstopi v polje večplastnega doživetja.<br>Pri projektu so bili razviti najnovejši vmesniki računalniškega vida, kar je omogočalo interaktivno komunikacijo v realnem času. Sistem je s kamero zaznal obraz obiskovalca in začel interakcijo s podajanjem navodil. Na podlagi barv – vsak okus je imel namreč svojo barvo – je prepoznal obiskovalčevo izbiro in mu ponudil določen video posnetek.<br>Projekt je nastal v sodelovanju z Borutom Batageljem v Laboratoriju za računalniški vid, programer: Marko Marčetič.',
            "opisProjektaEng"   : 'Tasting different foods, the visitors choose from among four pairs of tastes – sweet-salty, sour-salty, hot-salty and bitter-salty – using two words to describe the individual food.<br>Participating in the project enables them to have a multilayered experience by cognitively processing tastes and interpreting them linguistically.<br>Advanced computer vision interfaces were developed as part of this project, to allow real-time interactive communication. With the aid of a camera the system recognized the visitor’s face and started the interaction by issuing instructions. Recognizing the visitor’s choice by color (the tastes had different colors) the system then offered a matching video clip.<br>The project was realized in collaboration with Borut Batagelj from the Computer Vision Laboratory, programmer: Marko Marčetič.',
            "lokacijaVidea"     : '',
        }
    },
    "room_7_45":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "E-knjižni nomad / Mobile E-Book Flaneur (2010/2011)",
            "projectTitle"      : "E-knjižni nomad / Mobile E-Book Flaneur (2010/2011)",
            "gradivoProjekta"   : 'interaktivne instalacije / interactive installations',
            "opisProjektaSlo"   : 'Draganovi performansi in happeningi se od leta 2005 dalje nanašajo na digitalno branje kot nomadsko prakso. V projektih se bralec sprehaja skozi podatkovno zbirko jezikovnega korpusa, ki privre z interneta, spet drugič je bralec urbani nomad, kjer je mesto razplasteno na podatkovne zbirke kulturnih artefaktov.<br>Mesto je mogoče »brati« kot uprostorjenje besedila na zemljevidu, na ravni spomina in pozabe preteklih kulturnih dogodkov in, končno, prek branja literarnih besedil, ki so prikazana na prenosnih zaslonskih napravah.<br>(Narvika Bovcon) Projekt je nastal v sodelovanju z Borutom Batageljem iz Laboratorija za računalniški vid na Fakulteti za računalništvo in informatiko, programerja: Blaž Divjak, Klemen Možina.',
            "opisProjektaEng"   : 'Dragan’s Mobile E-Book Flaneur references digital reading as a nomadic practice: the reader strolls through the database of the linguistic corpus streaming from the internet in one instance, and on another occasion the reader is an urban nomad, where the city is layered with databases containing cultural artefacts.<br>The city can thus be “read” at the level of the spatialization of the text on the map, at the level of memory and oblivion of past cultural events, and finally at the level of reading the literary texts displayed on mobile screen devices.<br>(Narvika Bovcon) The project was realized in collaboration with Borut Batagelj from Computer Vision Laboratory at the Faculty of Computer and Information Science, programmers: Blaž Divjak, Klemen Možina.',
            "lokacijaVidea"     : '',
        }
    },
    "room_7_46":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "Sonifikacija podobe I / Paradigm of Image Sonification 1 (2014)",
            "projectTitle"      : "Sonifikacija podobe I / Paradigm of Image Sonification 1 (2014)",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation',
            "opisProjektaSlo"   : 'Projekt je zamišljen kot umetniška raziskava, ki razširja možnosti pretvorbe zvoka v sliko s pretvorbo video skripta podobe in pretvorbe realnega snemanega dogodka v zvočni zapis – slišati videno. Delo se navezuje na eksperiment Atlas v Cernu. Za umetniško raziskavo je ključnega pomena razkriti entiteto zvoka kot značilno prezentacijo podatkov, ki sporočajo nekaj, česar drugače ne bi mogli izvedeti.<br>Sodelavci: prof. dr Franc Solina, FRI, dr. Borut Batagelj, FRI, programerji Gašper Kojek, Grega Štravs, Jošt Lajovec, Jan Meznarič, Miloš Lukić, FRI, mag. Matjaž Stepičnik s sodelavci iz Jedrskega reaktorja Instituta Jožef Stefan.',
            "opisProjektaEng"   : 'Conceived as artistic research, the project explores the possibilities of converting sound into images through the conversion of a video of an image, and of converting a filmed event into a sound recording – to hear the seen. The project refers to the Atlas experiment at Cern. What is crucial for this artistic research is to show the entity of sound as a characteristic presentation of data communicating something that could not otherwise be learned.<br>Collaborators: Prof. Franc Solina, PhD (Faculty of Computer and Information Science –FCIS), Borut Batagelj, PhD (FCIS), programmers Gašper Kojek Grega Štravs, Jošt Lajovec, Jan Meznarič and Miloš Lukić (FCIS), Matjaž Stepičnik, MSc, and colleagues (Nuclear Reactor at the Jožef Stefan Institute).',
            "lokacijaVidea"     : '',
        }
    },
    "room_7_47":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "Metamorfoza lingvistika / Linguistic Metamorphosis (2007/2010)",
            "projectTitle"      : "Metamorfoza lingvistika / Linguistic Metamorphosis (2007/2010)",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation',
            "opisProjektaSlo"   : 'Projekt nagovarja uporabnika k verbalizaciji čustvene izkušnje opozicije ter njeni pretvorbi v okviru slovničnih pravil slovenskega ali angleškega jezika kot inventarja simbolnih enot, ki določajo shemo na stopnji predupodabljanja. Udeleženčev lastni izraz, ki odseva njegovo mentalno in fizično pozicijo, se vzpostavi v relaciji do družbenega, psihološkega in kulturnega konteksta sporazumevanja.',
            "opisProjektaEng"   : 'The project invites users to verbalize their emotional experience of opposition, transforming it within the grammatical rules of Slovene or English as inventories of symbolic units determining the schema at the level of pre-representation. Reflecting their mental and physical positions, the participants’ expressions are formulated in relation to their social, psychological, and cultural contexts of communication.',
            "lokacijaVidea"     : '',
        }
    },
    "room_7_50":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Konceptna tabla XII / Concept Board 12",
            "projectTitle"      : "Konceptna tabla XII / Concept Board 12 - Srečo Dragan",
            "gradivoProjekta"   : '',
            "opisProjektaSlo"   : 'Retro metoda postmodernizma, video instalacija nove podobe, drugi vstop v umetnost galerijskih postavitev.<br>Od leve proti desni:<br>Nuša & Srečo Dragan, video instalacija na mednarodni razstavi Provokacija medija ‘80, Galerija DSLU, Ljubljana 1982,<br>Nuša & Srečo Dragan, Spomenik žrtev in orgije, video instalacija, Likovno razstavišče Rihard Jakopič, Ljubljana 1985, in na razstavi Pogled na 80. godine, Collegium Artisticum, Sarajevo l985,<br>Nuša & Srečo Dragan, Killing the Image, video instalacija, festival Video CD, Cankarjev dom, Ljubljana 1983.',
            "opisProjektaEng"   : 'Retro method of postmodernism, video installation of new image, second foray into the art of gallery settings.<br>From left to right:<br>Nuša & Srečo Dragan, video installation at the international exhibition Provocation of the Medium ‘80, DSLU Gallery, Ljubljana 1982,<br>Nuša & Srečo Dragan, Monument of Sacrifice and Orgy, video installation, Rihard Jakopič Gallery, Ljubljana 1985, and at the exhibition The View of the 80’s, Collegium Artisticum, Sarajevo 1985,<br>Nuša & Srečo Dragan, Killing the Image, video installation, Video CD festival, Cankarjev dom, Ljubljana 1983.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/7-50__05_ACTIONRECEPTION_ogled-OUTLINES.png',
            "klas"              : 'video-image750',
            "klas2"              : ''
        }
    },
    "room_9_51":{
        "type":"videoPhotoBackground",
        "data":{
            "avtor"             : "Avtor: Srečo Dragan",
            "naslovProjekta"    : "Multimedijska konceptna tabla XVIII / Multimedia Concept Board 18",
            "projectTitle"      : "Multimedijska konceptna tabla XVIII / Multimedia Concept Board 18 - Srečo Dragan",
            "gradivoProjekta"   : '',
            "opisProjektaSlo"   : 'Multimedijski portal kot koncept umetniškega raziskovanja mobilne platforme v realnem in virtualnem svetu. Prvič predstavljen leta 2011 na razstavi Muzej robotov v Umetnostni galeriji Maribor. Delo je darilo avtorja novonastajajoči zbirki robotske umetnosti pri nas.',
            "opisProjektaEng"   : 'A multimedia port as a concept of artistic research of a mobile platform in the real and virtual worlds. First presented at the Robot Museum exhibition at the Art Gallery Maribor in 2011. The work was a donation by the artist to the newly established collection of robot art in Slovenia.',
            "lokacijaVidea"     : '',
            "lokacijaSlike"     : 'img/9_51-DRAGAN-robotica-maribor-english.png',
            "klas"              : 'video-image951',
            "klas2"              : ''
        }
    },
    "room_9_52_1":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "Sonifikacija podobe III / Paradigm of Image Sonification 3 (2016)",
            "projectTitle"      : "Sonifikacija podobe III / Paradigm of Image Sonification 3 (2016)",
            "gradivoProjekta"   : 'interaktivna instalacija / interactive installation',
            "opisProjektaSlo"   : 'Projekt je nastal v sodelovanju z Laboratorijem za umetno inteligenco Fakultete za računalništvo in informatiko v Ljubljani. Umetniška raziskava razširja dosedanje možnosti prenosa slike in poteka med lokacijama FRI in MSUM.<br>Programerja: Gašper Kojek, Grega Štravs.',
            "opisProjektaEng"   : 'The project was made in collaboration with the Artificial Intelligence Laboratory at the Faculty of Computer and Information Science in Ljubljana. Exploring the possibilities of image transfer, the project takes place between the Faculty of Computer and Information Science and the Museum of Contemporary Art Metelkova.<br>Programmers: Gašper Kojek, Grega Štravs.',
            "lokacijaVidea"     : '',
        }
    },
    "room_9_52_2":{
        "type":"video",
        "data":{
            "avtor"             : '',
            "naslovProjekta"    : "Sonifikacija podobe II / Paradigm of Image Sonification 2 (2015)",
            "projectTitle"      : "Sonifikacija podobe II / Paradigm of Image Sonification 2 (2015)",
            "gradivoProjekta"   : 'interaktivni performans / interactive performance<br><p style="margin-top: -0.5%; font-size:90%;">Speculum Artium, Delavski dom Trbovlje</p>',
            "opisProjektaSlo"   : '',
            "opisProjektaEng"   : '',
            "lokacijaVidea"     : '',
        }
    },    
}